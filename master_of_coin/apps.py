from django.apps import AppConfig


class MasterOfCoinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'master_of_coin'
