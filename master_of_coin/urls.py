from django.urls import path
from . import views


urlpatterns = [
    path('balance/', views.get_balance, name='balance'),
    path('create_payment/', views.create_payment, name='create_payment'),
]
