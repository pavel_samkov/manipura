from django.shortcuts import render
from .models import Account, Payment
from django.http import JsonResponse
import datetime


def get_balance(request):
	account = Account.objects.get(id=request.user.id)
	return JsonResponse({'balance': account.balance})



def create_payment(request):
	my_account = Account.objects.get(id=request.user.id)
	payment = Payment.objects.create(
		account = my_account,
		amount = -100,
		date = datetime.datetime.now
		)
	if payment.amount < 0 and my_account.balance + payment.amount < 0:
		return JsonResponse({'error': 'Not enough funds'})
	payment.save()
	my_account.balance += payment.amount
	my_account.save()

	return JsonResponse({'balance': my_account.balance})