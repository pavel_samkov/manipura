from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, db_index=True)
	balance = models.DecimalField(max_digits=10, decimal_places=2)

	def __str__(self):
		return str(self.id)

class Payment(models.Model):
	account = models.ForeignKey(Account, on_delete=models.CASCADE)
	amount = models.DecimalField(max_digits=10, decimal_places=2)
	date = models.DateField(auto_now_add=True)